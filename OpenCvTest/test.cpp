#include <opencv2\opencv.hpp>
//#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <iostream>
#include <Windows.h>
#include <conio.h>
#include <thread>
#include <chrono>
#include <stdlib.h>

//Namespaces used.
using namespace cv;
using namespace std;
int nb = 0;
bool found = false;
bool active = true;
Point pinataco(150, 1180);
int nb_bonbons = 0;
bool incr_bonbon = false;
Point p_center(0, 0);


void impact(int x, int y, Point userdata)
{

	Point mp = userdata;
	int valx = mp.x;
	int valx2 = mp.x + 200;
	int valy = mp.y;
	int valy2 = mp.y + 200;
	cout << " (" << valx << ", " << valy << ")" << endl;

	if (y > valx && y < valx2 && x > valy && x < valy2)
	{
		found = true;
		nb = 2;
		cout << "on target - position (" << x << ", " << y << ")" << endl;
		for (int v = 0; v < 10000; v++) { int b = v; b++; }


	}
	else if (nb > 0) { found = true; }
	else if (found == true && nb == 0) {
		found = false;
		incr_bonbon = true;
		pinataco.x = 100 + (rand() % static_cast<int>(600 - 100 + 1));
		pinataco.y = 100 + (rand() % static_cast<int>(1100 - 100 + 1));
	}


}

void generateRandomPositions() {
	while (1) {
		pinataco.x = 100 + (rand() % static_cast<int>(600 - 100 + 1));
		pinataco.y = 100 + (rand() % static_cast<int>(1100 - 100 + 1));
		this_thread::sleep_for(chrono::seconds(5));
	}
}
void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
	if (event == EVENT_MOUSEMOVE && flags == EVENT_FLAG_CTRLKEY)
		active = false;
	else
		active = true;



}
void showBonbon(Mat frame, Mat img, Size s, Point p) {
	Point2i draw_position(p.y, p.x);


	for (int y = 0; y < img.rows; ++y)
	{
		for (int x = 0; x < img.cols; ++x)
		{
			Vec4b & pixel = img.at<Vec4b>(y, x);
			Vec3b & pixel_dst = frame.at<Vec3b>(y + draw_position.y - s.height / 2, x + draw_position.x - s.width / 2);
			pixel_dst = (pixel[3] / 255.0f) * Vec3b(pixel[0], pixel[1], pixel[2]) + (1 - pixel[3] / 255.0f) * pixel_dst;
		}
	}
	this_thread::sleep_for(chrono::seconds(3));
}
//Main function.
int  main(int argc, char** argv)
{
	char no[50];
	Point coordo(0, 0);
	Size size = Size(200, 200);
	//Open any webcam.
	VideoCapture webcam(0);
	webcam.set(CV_CAP_PROP_FRAME_WIDTH, 1366);
	webcam.set(CV_CAP_PROP_FRAME_HEIGHT, 768);

	//If the webcam does not open, terminate program.
	if (!webcam.isOpened())
	{
		cerr << "[-] Error: Unable to open the webcam.\n" << endl;
		cerr << "Press ENTER to exit." << endl;
		cin.ignore();
		return EXIT_FAILURE;
	}

	//Set position of the console.
	HWND console = GetConsoleWindow();
	SetWindowPos(console, 0, 0, 470, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

	//Create windows.
	namedWindow("Input", CV_WINDOW_NORMAL);
	namedWindow("Output", CV_WINDOW_NORMAL);
	namedWindow("Binary", CV_WINDOW_NORMAL);
	namedWindow("Adjust", CV_WINDOW_NORMAL);

	//Set position of the windows.
	moveWindow("Input", 800, 25);
	moveWindow("Output", 0, 0);
	moveWindow("Binary", 1100, 25);
	moveWindow("Adjust", 800, 250);

	//Resize windows.
	resizeWindow("Input", 200, 150);
	resizeWindow("Binary", 200, 150);
	resizeWindow("Adjust", 500, 500);
	resizeWindow("Output", 1366, 768);

	int minH = 0;
	int maxH = 255;

	int minS = 0;
	int maxS = 255;

	int minV = 0;
	int maxV = 255;

	//Create trackbars to modify variables and find the desired color.
	cvCreateTrackbar("Min-H", "Adjust", &minH, 36);
	cvCreateTrackbar("Max-H", "Adjust", &maxH, 86);

	cvCreateTrackbar("Min-S", "Adjust", &minS, 200);
	cvCreateTrackbar("Max-S", "Adjust", &maxS, 255);

	cvCreateTrackbar("Min-V", "Adjust", &minV, 25);
	cvCreateTrackbar("Max-V", "Adjust", &maxV, 255);

	Mat pinata = imread("pinata.png", IMREAD_UNCHANGED);
	Mat bonbon = imread("w.png", IMREAD_UNCHANGED);
	resize(pinata, pinata, Size(100, 100));
	resize(bonbon, bonbon, Size(100, 100));

	char checkKeyPressed = 0;
	cout << "Press ESC to exit..." << endl;
	thread positions(&generateRandomPositions);
	while (checkKeyPressed != 27 && webcam.isOpened())
	{

		setMouseCallback("Output", CallBackFunc, NULL);
		if (nb == 0) { found = false; }
		nb--;
		//Get a frame of the webcam.
		Mat frm_original;
		webcam >> frm_original;
		flip(frm_original, frm_original, 1);
		if (found == false)
		{
			resize(pinata, pinata, size, 0.0, 0.0, INTER_AREA);
			Point2i draw_position(pinataco.y, pinataco.x);


			for (int y = 0; y < pinata.rows; ++y)
			{
				for (int x = 0; x < pinata.cols; ++x)
				{
					Vec4b & pixel = pinata.at<Vec4b>(y, x);
					Vec3b & pixel_dst = frm_original.at<Vec3b>(y + draw_position.y - size.height / 2, x + draw_position.x - size.width / 2);
					pixel_dst = (pixel[3] / 255.0f) * Vec3b(pixel[0], pixel[1], pixel[2]) + (1 - pixel[3] / 255.0f) * pixel_dst;
				}
			}
			coordo = pinataco;

		}
		else if (found == true) {/////////////// ajouter des condisitons
			if (incr_bonbon == true) { nb_bonbons++; incr_bonbon = false; found = false; }
			std::thread v(showBonbon, frm_original, bonbon, size, coordo);
			v.detach();


		}

		sprintf(no, "%d Bonbons", nb_bonbons);
		putText(frm_original, no, Point(10, 30), FONT_HERSHEY_TRIPLEX, 1, Scalar(255, 255, 255), 1);



		if (frm_original.empty())
		{
			cerr << "[-] Error: Unable to read the next frame.\n" << endl;
			cerr << "Press ENTER to exit." << endl;
			cin.ignore();
			break;
		}

		imshow("Input", frm_original);

		//Convert frame from RGB to HSV.
		Mat frm_hsv, frm_binary;
		cvtColor(frm_original, frm_hsv, CV_BGR2HSV);

		//Apply desired color filter.
		inRange(frm_hsv, Scalar(minH, minS, minV), Scalar(maxH, maxS, maxV), frm_binary);

		//Apply Morphological transformations. (extracted region of interest)
		Mat element = getStructuringElement(MORPH_RECT, Size(20, 20));
		erode(frm_binary, frm_binary, element);
		dilate(frm_binary, frm_binary, element);

		imshow("Binary", frm_binary);

		//Search contours in the binary image.
		vector< vector<Point> > contours;
		findContours(frm_binary, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

		//Draw rectangle and text in coordinates (x, y


		for (vector<Point> contour : contours)
		{
			Rect r = boundingRect(contour);
			rectangle(frm_original, r.tl(), r.br(), Scalar(0, 255, 0), 1, CV_AA, 0);
			Point pt_center(r.x + (r.width / 2) + 5, r.y + (r.height / 2) + 5);
			if (active)
			{
				SetCursorPos(pt_center.x, pt_center.y);
				cout << "Point: " << pt_center.x << "," << pt_center.y << endl;
			}
			p_center = pt_center;
		}

		imshow("Output", frm_original);

		impact(p_center.x, p_center.y, coordo);
		checkKeyPressed = waitKey(27);
	}

	webcam.release();
	destroyAllWindows();


	return EXIT_SUCCESS;
}